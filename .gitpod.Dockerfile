# TODO: Use ghcr.io/madebythepinshub/gitpodified-dotfiles/base in the future
FROM gitpod/workspace-full

# This environment var will be used in the runtime for our scripts/deploy-config.sh for testing
ENV GITPODIFY=true \
    # Add these scripts into our PATH
    PATH=$PATH:/workspace/matrix-org/rtapp-homeserver-config/scripts:/workspace/matrix-org/envs-net-homeserver-config/usr/local/bin
