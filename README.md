# Matrix Homeserver Configuration

We store all Matrix homeserver configuration in this repo, so anyone can use it and also adopt based on your needs for your next Matrix homeserver.
You can also deposit your Matrix configuration here for archival purposes and other sutff, as long as we and this repository exists. (Don't like us? No problem, feel free to fork. No hard feeling from us will be ever sent to your inbox.)

## Available scripts

These scripts are available at [`script`](./scripts) directory. You can specify the path to local copy of this repository via `GITPOD_ROOT_REPO` shell variable if `GITPODIFY` is set to `true` so you can enter an homeserver's server name instead.

### [`deploy-config.sh`](./scripts/deploy-config.sh)

Deploy an directory containing homeserver configuration template into an specific directory. Currently under development.

**Usage**: `[GITPODIFY=true GITPOD_ROOT_REPO=/path/to/local/repo] deploy-script.sh </path/to/target-directory> </path/to/template-config|hs-server-name>` (assuming its on PATH)

**Supported variables**: Run the script with `--help` for the full list

### [`backup-config.sh`](./scripts/backup-config.sh)

Copy your current configuration files into an `config/<server-name>` directory on your local copy of the repo.

**Usage**: `[GITPODIFY=true GITPOD_ROOT_REPO=/path/to/local/repo] backup-config.sh </path/to/config-dir> </path/to/backup/dir|hs-server-name`
