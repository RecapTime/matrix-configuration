#!/bin/bash
#shellcheck disable=SC2034,SC2269

usage="""Usage: $0 <source-template-dir> <target-path>

Deploy recaptime.im's Synapse homeserver configuration (or your HS configurations) to your 
own servers orjust creating a new one based on our configuration, which is also work as an 
template for new Matrix homeservers. You should run this script on an directory where you 
clone the Git repository of our/your homeserver configurations or atleast you wget'd
the tarball/archive somewhere. To change the server name and base URL, set MATRIX_SERVER_NAME
and MATRIX_PUBLIC_BASE_URL variables.

Required variables:
  - PGHOST, PGPORT, PGUSER, PGPASSWORD, PGDATABASE - Postgres connection configuration (required)
  - REGISTRATION_SHARED_SECRET - Registration shared secret that can be used in an special script or
    API endpoint, even signups are disabled. (generates an 64 character secret with OpenSSL CLI)
  - SENTRY_DSN - Sentry DSN (uses the default one defined in script, which you should comment out
    Sentry sutff on your generated config file immediately after running this script)
  - GITHUB_HOST, GITHUB_CLIENT_ID, GITHUB_CLIENT_SECRET - GitHub OAuth stuff (if GITHUB_HOST is blank,
    fallbacks to the default GitHub.com, required)
  - GITLAB_HOST, GITLAB_CLIENT_ID, GITLAB_CLIENT_SECRET - GitLab OAuth sutff (if GITLAB_HOST is blank,
    falls back to GitLab SaaS, required)
"""

if [[ $1 == "" ]] || [[ $1 == "help" ]] || [[ $1 == "-h" ]] || [[ $1 == "--help" ]]; then
  echo "$usage"
  exit 0 # Just bail out gracefully
fi

## Target directory and server name ##
MATRIX_SERVER_NAME=${MATRIX_SERVER_NAME:"recaptime.im"}
if [[ $GITPODIFY == "true" ]]; then
  MATRIX_SOURCE_CONFIG_DIR="$GITPOD_REPO_ROOT/config/$2"
  MATRIX_DEPLOYMENT_DIRECTORY=${1:-"$GITPOD_REPO_ROOT/config/$MATRIX_SERVER_NAME"}
else
  MATRIX_SOURCE_CONFIG_DIR=$2
  MATRIX_DEPLOYMENT_DIRECTORY=${1:"$HOME/.config/tmp/$MATRIX_SERVER_NAME"}
fi

## Postgres ##
PGUSER=${PGUSER:-"matrix"}
PGPASSWORD=${PGPASSWORD:-"matrix"}
PGHOST=${PGHOST:-"localhost"}
PGDATABASE=${PGDATABASE:-"matrix_synapse"}
PGHOST=${PGHOST:-"5432"}

## Registration secret ##
REGISTRATION_SHARED_SECRET=${REGISTRATION_SHARED_SECRET:-"$(openssl rand -hex 69)"}

## Sentry ##
SENTRY_DSN=${SENTRY_DSN:-""}

## OpenID Connect stuff ##
# GitHub - Note that GitHub is not an OIDC-compliant OAuth IdP, so we'll do some tricks here.
GITHUB_HOST=${GITHUB_HOST:-"github.com"}
GITHUB_CLIENT_ID=${GITHUB_CLIENT_ID}
GITHUB_CLIENT_SECRET=${GITHUB_CLIENT_SECRET}
# GitLab
GITLAB_HOST=${GITLAB_HOST:-"https://gitlab.com"}
GITLAB_CLIENT_ID=${GITLAB_CLIENT_ID}
GITLAB_CLIENT_SECRET=${GITLAB_CLIENT_SECRET}